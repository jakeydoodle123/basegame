Note: The launcher jar can be found here: launcher/out/artifacts/BaseLauncher_jar/BaseLauncher.jar
# basegame

Basegame by: Jacob Mealey


Basegame is a small skeleton of an RPG where players can have a "game master" of sorts who can run the game.

to do:
- reformat whole server to be more pythonic 
  - abstract all of the c-like code into an easy to use socket class
  - implement actual OO 
- redo the crazy long if/else tree 
- More with JSON and custom events 


p.s. 
Base socket code was originally written by Binarytides user: Silver Moon. http://www.binarytides.com/code-chat-application-server-client-sockets-python/
